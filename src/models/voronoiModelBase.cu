#define NVCC
#define ENABLE_CUDA

#include <cuda_runtime.h>
#include "indexer.h"
#include "gpubox.h"
#include "functions.h"
#include <iostream>
#include <stdio.h>
#include "voronoiModelBase.cuh"

/*! \file voronoiModelBase.cu */
/*!
    \addtogroup voronoiModelBaseKernels
    @{
*/

/*!
  Since the cells are guaranteed to be convex, the area of the cell is the sum of the areas of
  the triangles formed by consecutive Voronoi vertices
  */
__global__ void gpu_compute_voronoi_geometry_kernel(const Dscalar2* __restrict__ d_points,
                                          Dscalar2* __restrict__ d_AP,
                                          const int* __restrict__ d_nn,
                                          const int* __restrict__ d_n,
                                          Dscalar2* __restrict__ d_vc,
                                          Dscalar4* __restrict__ d_vln,
                                          int N,
                                          Index2D n_idx,
                                          gpubox Box
                                        )
    {
    // read in the particle that belongs to this thread
    unsigned int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= N)
        return;

    Dscalar2  nnextp, nlastp,pi,rij,rik,vlast,vnext,vfirst;

    int neigh = d_nn[idx];
    Dscalar Varea = 0.0;
    Dscalar Vperi= 0.0;

    pi = d_points[idx];
    nlastp = d_points[ d_n[n_idx(neigh-1,idx)] ];
    nnextp = d_points[ d_n[n_idx(0,idx)] ];

    Box.minDist(nlastp,pi,rij);
    Box.minDist(nnextp,pi,rik);
    Circumcenter(rij,rik,vfirst);
    vlast = vfirst;

    //set the VoroCur to this voronoi vertex
    //the convention is that nn=0 in this routine should be nn = 1 in the force sets calculation
    d_vc[n_idx(1,idx)] = vlast;

    for (int nn = 1; nn < neigh; ++nn)
        {
        rij = rik;
        int nid = d_n[n_idx(nn,idx)];
        nnextp = d_points[ nid ];
        Box.minDist(nnextp,pi,rik);
        Circumcenter(rij,rik,vnext);

        //fill in the VoroCur structure

        int idc = n_idx(nn+1,idx);
        if(nn == neigh-1)
            idc = n_idx(0,idx);

        d_vc[idc]=vnext;

        //...and back to computing the geometry
        Varea += TriangleArea(vlast,vnext);
        Dscalar dx = vlast.x - vnext.x;
        Dscalar dy = vlast.y - vnext.y;
        Vperi += sqrt(dx*dx+dy*dy);
        vlast=vnext;
        };
    Varea += TriangleArea(vlast,vfirst);
    Dscalar dx = vlast.x - vfirst.x;
    Dscalar dy = vlast.y - vfirst.y;
    Vperi += sqrt(dx*dx+dy*dy);

    //it's more memory-access friendly to now fill in the VoroLastNext structure separately
    vlast = d_vc[n_idx(neigh-1,idx)];
    vfirst = d_vc[n_idx(0,idx)];
    for (int nn = 0; nn < neigh; ++nn)
        {
        int idn = n_idx(nn+1,idx);
        if(nn == neigh-1) idn = n_idx(0,idx);
        vnext = d_vc[idn];

        int idc = n_idx(nn,idx);
        d_vln[idc].x = vlast.x;
        d_vln[idc].y = vlast.y;
        d_vln[idc].z = vnext.x;
        d_vln[idc].w = vnext.y;

        vlast = vfirst;
        vfirst = vnext;
        };

    d_AP[idx].x=Varea;
    d_AP[idx].y=Vperi;

    return;
    };

//!Call the kernel to compute the geometry
bool gpu_compute_voronoi_geometry(Dscalar2 *d_points,
                        Dscalar2   *d_AP,
                        int      *d_nn,
                        int      *d_n,
                        Dscalar2 *d_vc,
                        Dscalar4 *d_vln,
                        int      N,
                        Index2D  &n_idx,
                        gpubox &Box
                        )
    {
    unsigned int block_size = NUMTHREADS;
    if (N < 128) block_size = 32;
    unsigned int nblocks  = N/block_size + 1;

    gpu_compute_voronoi_geometry_kernel<<<nblocks,block_size>>>(
                                                d_points,
                                                d_AP,
                                                d_nn,
                                                d_n,
                                                d_vc,
                                                d_vln,
                                                N,
                                                n_idx,
                                                Box
                                                );
    HANDLE_ERROR(cudaGetLastError());
    return cudaSuccess;
    };


/** @} */ //end of group declaration
