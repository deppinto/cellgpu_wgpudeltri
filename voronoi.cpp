#include "std_include.h"

#include "cuda_runtime.h"
#include "cuda_profiler_api.h"

#define ENABLE_CUDA

#include "Simulation.h"
#include "voronoiQuadraticEnergy.h"
#include "selfPropelledParticleDynamics.h"
#include "DatabaseNetCDFSPV.h"
#include "speckle.h"

/*!
This file compiles to produce an executable that can be used to reproduce the timing information
in the main cellGPU paper. It sets up a simulation that takes control of a voronoi model and a simple
model of active motility
*/
int main(int argc, char*argv[])
{
    //...some default parameters
    int numpts = 131072; //number of cells
    int USE_GPU = 0; //0 or greater uses a gpu, any negative number runs on the cpu
    int c;
    int tSteps = 0; //number of time steps to run after initialization
    int initSteps = 0; //number of initialization steps

    Dscalar dt = 0.001; //the time step size
    Dscalar p0 = 3.9;  //the preferred perimeter
    Dscalar a0 = 1.0;  //the preferred area
    Dscalar v0 = 0.3;  //the self-propulsion
    Dscalar Dr = 1.0;  //the rotational diffusion
    int dx=1024;
    int program_switch = -1; //various settings control output
    int SubIntType = 0;
    Dscalar IncrementationValue=0.0; 
    bool rnd_Sub=false;
    Dscalar tau=10000000000;

    double sigmac = 0;

    //The defaults can be overridden from the command line
    while((c=getopt(argc,argv,"n:g:m:s:r:a:i:v:b:x:y:z:p:t:e:w:l:")) != -1)
        switch(c)
        {
            case 'n': numpts = atoi(optarg); break;
            case 't': tSteps = atoi(optarg); break;
            case 'g': USE_GPU = atoi(optarg); break;
            case 'i': initSteps = atoi(optarg); break;
            case 'e': dt = atof(optarg); break;
            case 'p': p0 = atof(optarg); break;
            case 'a': a0 = atof(optarg); break;
            case 'v': v0 = atof(optarg); break;
            case 's': SubIntType = atoi(optarg); break;
            case 'z': tau = atof(optarg); break;
            case 'r': IncrementationValue = atof(optarg); break;
            case 'w': dx = atoi(optarg); break;
            case 'm': tau = atof(optarg); break;
            case 'l': sigmac = atof(optarg); break;
            case '?':
                    if(optopt=='c')
                        std::cerr<<"Option -" << optopt << "requires an argument.\n";
                    else if(isprint(optopt))
                        std::cerr<<"Unknown option '-" << optopt << "'.\n";
                    else
                        std::cerr << "Unknown option character.\n";
                    return 1;
            default:
                       abort();
        };

    clock_t t1,t2; //clocks for timing information
    bool reproducible = true; // if you want random numbers with a more random seed each run, set this to false
    //check to see if we should run on a GPU
    bool initializeGPU = true;
    if (USE_GPU >= 0)
        {
        bool gpu = chooseGPU(USE_GPU);
        if (!gpu) return 0;
        cudaSetDevice(USE_GPU);
        }
    else
        initializeGPU = false;


    cudaProfilerStart();

    //possibly save output in netCDF format
    char dataname[256];
    sprintf(dataname,"./test_voronoi_St%d_T%g_Iv%g_P%g_A%g_V%g.nc", SubIntType, tau, IncrementationValue, p0, a0, v0);
    int Nvert = numpts;
    SPVDatabaseNetCDF ncdat(Nvert,dataname,NcFile::Replace);

    //define an equation of motion object...here for self-propelled cells
    EOMPtr spp = make_shared<selfPropelledParticleDynamics>(numpts);
    
    //define a voronoi configuration with a quadratic energy functional
    shared_ptr<VoronoiQuadraticEnergy> spv  = make_shared<VoronoiQuadraticEnergy>(numpts, reproducible);
    
    //set the cell preferences to uniformly have A_0 = 1, P_0 = p_0
    spv->setCellPreferencesUniform(a0,p0);
    //set the cell activity to have D_r = 1. and a given v_0
    spv->setv0Dr(v0,1.0);
    //set the substrate preferences values
    spv->setSubstratePreferences(0, 0.0, dx, tau, 0.0);
    //set the cell moduli preferences to uniformly have a given K_a and K_p
    spv->setModuliUniform(1.0, 1.0);

    //combine the equation of motion and the cell configuration in a "Simulation"
    SimulationPtr sim = make_shared<Simulation>();
    sim->setConfiguration(spv);
    sim->addUpdater(spp,spv);
    //set the time step size
    sim->setIntegrationTimestep(dt);
    //initialize Hilbert-curve sorting... can be turned off by commenting out this line or seting the argument to a negative number
    //sim->setSortPeriod(initSteps/10);
    //set appropriate CPU and GPU flags
    sim->setCPUOperation(!initializeGPU);
    sim->setReproducible(reproducible);

    //ncdat.WriteState(spv);
    //ncdat.ReadState(spv, 0);
    //run for a few initialization timesteps
    printf("starting initialization\n");
    for(int ii = 0; ii < initSteps; ++ii)
        {
	  //cout<<"SIM: "<<ii<<endl;
        sim->performTimestep();
        if(ii%100 ==0)
	  {
            printf("timestep %i\t\t energy %f \n",ii,spv->computeEnergy());
	  };
        };
    printf("Finished with initialization\n");
    cout << "current q = " << spv->reportq() << endl;
    //the reporting of the force should yield a number that is numerically close to zero.
    spv->reportMeanCellForce(true);
    double arguments[dx*dx];


    if(rnd_Sub==false)
      {
	spv->setSubstratePreferences(SubIntType, IncrementationValue, dx, tau, sigmac);
      }
    else{ 
        vector<vector<double> > randField(dx, vector<double>(dx,0));
        speckle rndfield;
        rndfield.randomField(randField, sqrt(numpts), dx, sigmac);

        for(int u=0; u<dx; u++){
            for(int k=0; k<dx; k++){
                int calc_site=k+u*dx;
                arguments[calc_site]=randField[k][u];
            }
        }
        spv->setSubstratePreferencesRnd(SubIntType, IncrementationValue, dx, arguments, tau);
    }

    
    switch(SubIntType)
    {
    case 1:
        spv->setCellPreferencesSubstrate(a0, p0, true, dx);
    break;
    case 2:
        spv->setCellPreferencesSubstrate(a0, p0, false, dx);
    break;
    case 3:
        spv->setv0DrSubstrate(v0, 1.0, dx);
    break;
    }
        
    ncdat.WriteState(spv);
    //run for additional timesteps, and record timing information
    t1=clock();
    int mult=10;
    int val=1;
    int value_calc=10;

    for(int ii = 0; ii < tSteps; ++ii)
      {

        if(ii%10000 ==0)
	  {
	    printf("timestep %i\t\t energy %f \n",ii,spv->computeEnergy());
	  };
        //ncdat.WriteState(spv);
        sim->performTimestep();
	
        if(program_switch <0)
	  {
	    int power=pow(10, val);
            if(ii%power==0){
              switch(val)
		{
		case 2:
		  mult=100;
		  break;
		case 3:
		  mult=200;
		  break;
		case 6:
		  mult=300;
		  break;
		case 7:
		  mult=600;
		  break;
		case 8:
		  mult=1200;
		  break;
		}
	      value_calc=(int)(pow(10, val+1)/mult);
	      val++;
            }
	    if(ii%value_calc==0)ncdat.WriteState(spv);
	  }

/*
	if(ii==initSteps){
	  //spv->setSubstratePreferences(SubIntType, IncrementationValue, dx, tau, sigmac);
	  //sim->setCurrentTime(0.0);
	  spv->setSubstratePreferences(SubIntType, 0.0, dx, tau, sigmac);
	  spv->setCellPreferencesSubstrate(a0, p0, false, dx);
	}
*/

      };
    t2=clock();
    Dscalar steptime = (t2-t1)/(Dscalar)CLOCKS_PER_SEC/tSteps;
    cout << "timestep ~ " << steptime << " per frame; " << endl;
    cout << spv->reportq() << endl;
    spv->reportMeanCellForce(true);

    if(initializeGPU)
        cudaDeviceReset();
    return 0;
};
