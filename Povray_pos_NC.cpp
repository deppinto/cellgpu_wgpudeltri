#include "std_include.h"

#include "cuda_runtime.h"
#include "cuda_profiler_api.h"

#define ENABLE_CUDA

#include "Simulation.h"
#include "voronoiQuadraticEnergy.h"
#include "selfPropelledParticleDynamics.h"
#include "DatabaseNetCDFSPV.h"
#include "speckle.h"
#include <cstring>

using namespace std;


int main(){

	int N=131072;
	int USE_GPU=0;

	bool reproducible = false;
	//check to see if we should run on a GPU
        bool initializeGPU = true;
        if (USE_GPU >= 0)
        {
        bool gpu = chooseGPU(USE_GPU);
        if (!gpu) return 0;
        cudaSetDevice(USE_GPU);
        }
        else
        initializeGPU = false;

	EOMPtr spp = make_shared<selfPropelledParticleDynamics>(N);
        shared_ptr<VoronoiQuadraticEnergy> spv = make_shared<VoronoiQuadraticEnergy>(N, reproducible);
	SimulationPtr sim = make_shared<Simulation>();
	sim->setConfiguration(spv);
	sim->addUpdater(spp,spv);
	sim->setCPUOperation(!initializeGPU);
	sim->setReproducible(reproducible);


        char fn[256];
        sprintf(fn,"failedPos.txt");
        ifstream output(fn);
        spv->readTriangulation(output);

    return 0;
}
